#!/usr/bin/python

# This is a sample client to ilustrate how use the tworoads module
# package to download a file from Amazon S3 service

import sys
from tworoads import S3

# Create a tworoads.S3 object
s3 = S3()

# Get the files from S3 (target directory /tmp/acl-coling-2014-about-ask should exist)
result = s3.get_file('testcodenvy', 'acl-coling-2014/about_ask.xml', '/tmp/acl-coling-2014-about-ask')
print("RESULT: " + str(result))
