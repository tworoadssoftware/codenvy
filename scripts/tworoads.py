#!/usr/bin/python

import sys
import boto
import configparser
import os.path

from boto.s3.key import Key
from boto.s3.connection import S3Connection

class S3:
    # Properties to store AWS credentials
    aws_access_key = ''
    aws_secret_key = ''
    conn = False

    # This function downloads a single file from a bucket on S3
    # Return values:
    #    0 if file is already there,
    #    1 if file was retrieved from bucket,
    #   -1 for error
    def get_file(self, bucket_name, file_path, directory):
        # Load aws credentials from /etc/aws.conf
        if not self.load_aws_credentials():
            return -1

        # Check that target directory exists
        if not os.path.isdir(directory):
            print("ERROR: Directory " + directory + " doesn't exists")
            return -1

        # Check if file was already downloaded
        file_name = file_path.rsplit('/', 1)[-1]
        target_location = directory + '/' + file_name
        if os.path.isfile(target_location):
            print('INFO: File ' + file_name + ' already exists on ' + directory)
            return 0

        # Download file
        print('INFO: Downloading file ' + file_path + ' from bucket ' + bucket_name + ' into directory ' + directory)
        bucket = self.conn.get_bucket(bucket_name)
        key = Key(bucket, file_path)
        key.get_contents_to_filename(target_location)

        return 1

    # This function uploads a single file to a bucket on S3
    # Return values:
    #    1 if file was uploaded (created or updated)
    #   -1 for error
    def write_file(self, bucket_name, bucket_path, file_path):
        # Load aws credentials from /etc/aws.conf
        if not self.load_aws_credentials():
            return -1

        # Check if file exists
        if not os.path.isfile(file_path):
            print('ERROR: Source file ' + file_path + " doesn't exists ")
            return -1

        # Check if bucket exits (otherwise create it)
        bucket = self.conn.lookup(bucket_name)
        if bucket is None:
            print('INFO: Creating bucket ' + bucket_name)
            self.conn.create_bucket(bucket_name)
            bucket = self.conn.lookup(bucket_name)

        # Upload file
        print('INFO: Uploading file ' + file_path + ' to bucket ' + bucket_name + ' on directory ' + bucket_path)
        file_name = file_path.rsplit('/', 1)[-1]
        key_name = os.path.join(bucket_path, file_name)
        key = bucket.new_key(key_name)
        key.set_contents_from_filename(file_path)

        return 1

    # This function downloads an entire directory from a bucket on S3
    def get_files(self, bucket_name, bucket_path, directory):
        # Load aws credentials from /etc/aws.conf
        if not self.load_aws_credentials():
            return -1

        # Check that target directory exists
        if not os.path.isdir(directory):
            print("ERROR: Directory " + directory + " doesn't exists")
            return False

        # Start import process
        print('INFO: Importing files from bucket ' + bucket_name + ' into directory ' + directory)
        path_len = len(bucket_path)
        bucket = self.conn.get_bucket(bucket_name)
        for key in bucket.list():
            bucket_file = key.name.encode('utf-8')
            # If the path of the bucket file starts with the
            # provided path, include that file
            bucket_file_preffix = bucket_file[0:path_len].decode('utf-8')
            if bucket_file_preffix == bucket_path:
                bucket_file = bucket_file.decode('utf-8')
                file_name = bucket_file.replace(bucket_file_preffix + '/', '')
                file_name = file_name.rsplit('/', 1)[-1]
                target_location = directory + '/' + file_name
                if not os.path.isfile(target_location):
                    print('Downloading ' + bucket_file)
                    key.get_contents_to_filename(target_location)

    # This function reads the AWS credentials
    def load_aws_credentials(self):
        # Check if config file with AWS credentials exists
        aws_cred = '/etc/aws.conf'
        if not os.path.isfile(aws_cred):
            print('ERROR: AWS credentials not found at ' + aws_cred)
            print('Please create the file with the following syntax:')
            print('[AWS]')
            print('access_key=xxxxxxxxxxxxxxx')
            print('secret_key=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
            return False

        # Load AWS credentials
        config = configparser.ConfigParser()
        config.read(aws_cred)
        self.access_key = config.get('AWS', 'access_key')
        self.secret_key = config.get('AWS', 'secret_key')

        # Open connection to S3
        self.conn = S3Connection(self.access_key, self.secret_key)

        return True
