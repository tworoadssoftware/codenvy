#!/usr/bin/python

# This is a sample client to ilustrate how use the tworoads module
# package to download files from the Amazon S3 service

import sys
from tworoads import S3

# Create a tworoads.S3 object
s3 = S3()

# Get all the files inside of the 'acl-coling-2014' directory on the 'testcodenvy' bucket
# (target directory /tmp/acl-coling-2014 should exist)
s3.get_files('testcodenvy', 'acl-coling-2014', '/tmp/acl-coling-2014')
